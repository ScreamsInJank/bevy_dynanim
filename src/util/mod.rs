/// Utility type for determining how the second order system should be applied to an object
pub enum EntityDynamicsApplyMode {
    /// Motion is applied directly to the target relative to the control transformation
    TransformRelative,
    /// Motion is applied directly using the control's global transformation
    TransformGlobal,
    /// Motion is translated using physics by setting velocity
    #[cfg(feature = "physics")]
    PhysGlobalVelocity,
    /// Motion is translated using physics by setting velocity
    #[cfg(feature = "physics")]
    PhysGlobalForce,
}