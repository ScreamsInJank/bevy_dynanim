use bevy::prelude::*;
use serde::*;

use super::util::DynamicsTargetParams;

#[derive(Component, Serialize, Deserialize)]
/// Component used to define entities which are used as control points to drive entities with DynamicsTarget
pub struct DynamicsControlPoint<T> {
    /// Set of entity ids which the controller influences
    pub targets: Vec<Entity>,
    pub input: T,
    pub prev_input: T,
    pub d_input: T,
}

#[derive(Component, Serialize, Deserialize)]
/// State component for second order dynamics on a given object
pub struct DynamicsTarget {
    /// The ID of the Entity which has the corresponding control bone
    pub controller_entity: Entity,
    /// The Previous Position Vector
    /// The Previous Velocity Vector
    /// Scaling factor applied to changes in position before application
    /// Axis values of +-1 should be used to enable an axis, values of 0 should be used to disable an axis
    pub position_factor: Vec3,
    /// Scaling factor applied to changes in rotation before application
    /// Axis values of +-1 should be used to enable an axis, values of 0 should be used to disable an axis
    pub rotation_factor: Vec3,

    /// Parameters used to tune dynamics, must be set using Self::set_params(..)
    pub params: DynamicsTargetParams,
}

impl DynamicsTarget {
    pub fn new(
        controller_entity: Entity,
        position_factor: Vec3,
        rotation_factor: Vec3,
        params: DynamicsTargetParams,
    ) -> Self {
        Self {
            controller_entity,
            position_factor,
            rotation_factor,
            params,
        }
    }
}

#[derive(Component, Default, Reflect, Serialize, Deserialize, Clone, Copy)]
/// Generic struct to store current state of a dynamics target
pub struct DynamicsTargetState<T> {
    pub output: T,
    pub d_output: T,
}
