use bevy::prelude::*;

use super::components::DynamicsControlPoint;

/// Updates stored velocities and positions of all control nodes each frame
pub fn process_control_translation_changes(
    time: Res<Time>,
    mut control_point_query: Query<
        (&mut DynamicsControlPoint<Vec3>, &Transform),
        Changed<Transform>,
    >,
) {
    for (mut control_point, control_point_transform) in control_point_query.iter_mut() {
        // TODO: Handle relative vs absolute input
        if control_point.input != control_point_transform.translation {
            // Derive velocity
            control_point.d_input =
                (control_point_transform.translation - control_point.input) / time.delta_seconds();
            // Record new previous input
            control_point.prev_input = control_point.input;
            // Record current position as new input
            control_point.input = control_point_transform.translation;
        }
    }
}

/// Updates stored angular velocities and rotations of all control nodes each frame
pub fn process_control_rotation_changes(
    time: Res<Time>,
    mut control_point_query: Query<
        (&mut DynamicsControlPoint<Quat>, &Transform),
        Changed<Transform>,
    >,
) {
    for (mut control_point, control_point_transform) in control_point_query.iter_mut() {
        if control_point.input != control_point_transform.rotation {
            // Derive velocity
            control_point.d_input =
                (control_point_transform.rotation - control_point.input) / time.delta_seconds();
            // Record new previous input
            control_point.prev_input = control_point.input;
            // Record current position as new input
            control_point.input = control_point_transform.rotation;
        }
    }
}
