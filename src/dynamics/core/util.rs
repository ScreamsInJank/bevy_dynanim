use std::f32::consts::PI;

use bevy::prelude::*;
use serde::{Deserialize, Serialize};

#[derive(Reflect, Serialize, Deserialize, Clone, Copy)]
pub struct DynamicsTargetParams {
    /// The target system's resonant frequency
    pub freq: f32,
    /// The target system's damping coefficient
    pub damping: f32,
    /// The target system's intial frequency response
    pub response: f32,

    pub(crate) k1: f32,
    pub(crate) k2: f32,
    pub(crate) k3: f32,

    #[cfg(feature = "solver_matched")]
    pub(crate) _w: f32,
    #[cfg(feature = "solver_matched")]
    pub(crate) _z: f32,
    #[cfg(feature = "solver_matched")]
    pub(crate) _d: f32,
}

/// Preset values for Second order Parameters
impl DynamicsTargetParams {
    /// Create a new set of SecondOrderParameters
    pub fn new(freq: f32, damping: f32, response: f32) -> Self {
        Self {
            freq,
            damping,
            response,

            // Set the parameters
            k1: damping / (PI * freq),
            k2: 1.0 / (2.0 * PI * freq) * (2.0 * PI * freq),
            k3: (response * damping) / (2.0 * PI * freq),

            // W
            #[cfg(feature = "solver_matched")]
            _w: 2.0 * PI * freq,
            // Z
            #[cfg(feature = "solver_matched")]
            _z: damping,
            // D
            #[cfg(feature = "solver_matched")]
            _d: 2.0 * PI * freq * (damping.powi(2) - 1.0).abs().sqrt(),
        }
    }

    /// Set the parameters
    pub fn set(&mut self, freq: f32, damping: f32, response: f32) {
        *self = Self {
            freq,
            damping,
            response,

            // Set the parameters
            k1: damping / (PI * freq),
            k2: 1.0 / (2.0 * PI * freq) * (2.0 * PI * freq),
            k3: (response * damping) / (2.0 * PI * freq),

            // W
            #[cfg(feature = "solver_matched")]
            _w: 2.0 * PI * freq,
            // Z
            #[cfg(feature = "solver_matched")]
            _z: damping,
            // D
            #[cfg(feature = "solver_matched")]
            _d: 2.0 * PI * freq * (damping.powi(2) - 1.0).abs().sqrt(),
        }
    }

    /// Typical mechanical system with anticipation
    pub fn preset_mechanical() -> Self {
        Self::new(1.0, 0.5, 2.0)
    }

    /// Equivalent to the smoothdamp function in unity
    pub fn preset_critical() -> Self {
        Self::new(1.0, 1.0, 0.0)
    }

    /// Equivalent to the smoothdamp function in unity
    pub fn preset_smoothdamp() -> Self {
        Self::preset_critical()
    }
}
