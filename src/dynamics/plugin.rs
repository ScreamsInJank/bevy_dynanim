use bevy::prelude::*;

use super::solvers;

pub struct DynamicsPlugin;

impl Plugin for DynamicsPlugin {
    fn build(&self, app: &mut App) {
        #[cfg(feature = "solver_default")]
        {
            app.add_systems(Update, solvers::default::dynamics_update_translation);
            app.add_systems(Update, solvers::default::dynamics_update_rotation);
        }
        #[cfg(feature = "solver_microstep")]
        {
            app.add_systems(Update, solvers::microstep::dynamics_update_translation);
            app.add_systems(Update, solvers::microstep::dynamics_update_rotation);
        }
        #[cfg(feature = "solver_matched")]
        {
            app.add_systems(Update, solvers::matched::dynamics_update_translation);
            app.add_systems(Update, solvers::matched::dynamics_update_rotation);
        }
    }
}
