use std::ops::{Add,Sub,Mul,Div};

pub(crate) fn second_order_dynamics_fast<T>(
    dt: f32,
    dt_crit: f32,
    gains: (f32,f32,f32),
    scaling_factor: T,
    prev_input: (T, Option<T>),
    input: (T, Option<T>),
    prev_output: (T, T),
    output: &mut (T, T),
    fast_approximation: bool,
)
where T: Mul<Output=T> + Mul<f32,Output = T> + Add<T,Output=T> + Sub<T,Output=T> + Div<f32,Output=T> + Copy
{
    // For the fast approximation we may artificially increase k2 such that the system does not destabilise with large timesteps
    // This is not particularly accurate when compared to processing large timesteps in multiple small iterations but does increase performance
    // For those interested the system is stable as long as dt < sqrt(4*k2+k1^2)-k1, which in terms of the parameters is dt < sqrt(4/((2*PI*freq)*(2*PI*params.freq))+(damping/(PI*freq))^2)+damping/(PI*freq)
    let k2 = (
        0.5 * dt * (dt + gains.0), // First solution
        dt * gains.0               // Second Solution
    );
    // Use the largest of the three possible values for k2 (i wish f32 implemented Ord)
    // Prioritising the most realistic
    // let k2_stable: f32 = if gains.1 > k2.0 && gains.1 > k2.1 {
    //     gains.1
    // } else if k2.0 > gains.1 && k2.0 > k2.1 {
    //     k2.0
    // } else {
    //     k2.1
    // };
    let k2_stable: f32 = gains.1.max(k2.0).max(k2.1);
    // Apply change to output
    output.0 = output.0 * dt;
    // Apply change to output's derivative
    output.1 = output.1 + scaling_factor*(input.0 + input.1.unwrap()*gains.2 - output.0 - output.1*gains.0)/k2_stable;
}

pub(crate) fn second_order_dynamics_accurate<T>(
    dt: f32,
    dt_crit: f32,
    gains: (f32,f32,f32),
    scaling_factor: T,
    //prev_input: (T, Option<T>),
    input: (T, Option<T>),
    //prev_output: (T, T),
    output: &mut (T, T)
)
where T: Mul<Output=T> + Mul<f32,Output = T> + Add<T,Output=T> + Sub<T,Output=T> + Div<f32,Output=T> + Copy
{
    // This implementation preserves system stability by breaking timesteps in multiple small iterations to increase accuracy but does come at a cost to perfomance
    // Compute number of iterations needed 
    let iterations: f32 = (dt/dt_crit).ceil();
    let dt: f32 = dt/iterations;
    for _ in 0..iterations as u8 {
        // Apply change to output
        output.0 = output.0 * dt;
        // Apply change to output's derivative
        output.1 = output.1 + scaling_factor*(input.0 + input.1.unwrap()*gains.2 - output.0 - output.1*gains.1)/gains.2;
    }
}

pub(crate) fn second_order_dynamics_hyperaccurate<T>(
    dt: f32,
    dt_crit: f32,
    // (k1,k2,k3)
    gains: (f32,f32,f32),
    // (w,z,d)
    laplace_params: (f32,f32,f32),
    scaling_factor: T,
    prev_input: (T, Option<T>),
    input: (T, Option<T>),
    prev_output: (T, T),
    output: &mut (T, T),
    fast_approximation: bool,
)
where T: Mul<Output=T> + Mul<f32,Output = T> + Add<T,Output=T> + Sub<T,Output=T> + Div<f32,Output=T> + Copy
{
    // This implementation prevents the jittering behaviour seen at high frequencies (caused by negative eigenvalues) by using laplace zero-pole matching to compute values for k1 and k2 every frame based on the timestep
    // I do not fully understand how this works, come ask me in/after 2025 :)
    let k1_stable: f32;
    let k2_stable: f32;
    if dt*laplace_params.0 < laplace_params.2 {
        k1_stable = gains.0;
        // Clamp k2 to maximum safe value for the current timestep
        k2_stable = gains.1.max(gains.0*(dt + dt.powi(2))).max(dt*gains.0);
    } else { // Use pole matching for more accurate results when the system is moving fast relative to the timestep
        let t1: f32     = (-laplace_params.1 * laplace_params.0 * dt).exp();
        let alpha: f32  = 2.0 * t1 * (if laplace_params.1 <= 1.0 {(dt * laplace_params.2).cos()} else {(dt * laplace_params.2).cosh()});
        let beta: f32   = t1.powi(2);
        let t2: f32     = dt / (1.0 + beta - alpha);
        k1_stable = (1.0-beta) * t2;
        k2_stable = dt*t2;
    }
    // Integrate position by velocity
    output.0 = output.0 * dt;
    // Integrate velocity by acceleration
    output.1 = output.1 + scaling_factor*(input.0 + input.1.unwrap()*gains.2 - output.0 - output.1*k1_stable)/k2_stable;
}
