use bevy::prelude::*;
use std::ops::{Add, Div, Mul, Sub};

use crate::dynamics::core::{
    components::{DynamicsControlPoint, DynamicsTarget, DynamicsTargetState},
    util::DynamicsTargetParams,
};

use super::DynamicsSolverTarget;

#[derive(Component)]
pub struct DefaultDynamicsSolverTarget;
impl DynamicsSolverTarget for DefaultDynamicsSolverTarget {}

pub fn dynamics_step<T>(
    dt: f32,
    params: &DynamicsTargetParams,
    input: T,
    d_input: T,
    output: &mut T,
    d_output: &mut T,
) where
    T: Mul<Output = T>
        + Mul<f32, Output = T>
        + Add<T, Output = T>
        + Sub<T, Output = T>
        + Div<f32, Output = T>
        + Copy,
{
    // Clamp the value to preserve stability at the cost of accuracy (shouldn't be necessary for fully implicit euler method)
    // let k2_stable = params.k2.max(0.5*dt*(dt + params.k1)).max(dt*params.k1);
    // Calculate the new output
    *output = output.clone() + *d_output * dt;
    // Calculate the new derivative of the output
    // *d_output = (d_output.clone()*k2_stable + (input - output.clone() + d_input*params.k3)*dt)/(k2_stable+params.k1*dt);
    *d_output = (d_output.clone() * params.k2
        + (input - output.clone() + d_input * params.k3) * dt)
        / (params.k2 + params.k1 * dt);
}

/// System to update target entities' translations
pub fn dynamics_update_translation(
    time: Res<Time>,
    control_point_query: Query<&DynamicsControlPoint<Vec3>>,
    mut target_query: Query<
        (
            &mut DynamicsTarget,
            &mut DynamicsTargetState<Vec3>,
            &mut Transform,
        ),
        With<DefaultDynamicsSolverTarget>,
    >,
) {
    for (target, mut target_state, mut target_transform) in target_query.iter_mut() {
        // Get the control point's translations
        if let Ok(control_point) = control_point_query.get(target.controller_entity) {
            let control_translation = control_point.input;
            let control_velocity = control_point.d_input;

            let target_position = &mut target_transform.translation;
            let target_velocity = &mut target_state.d_output;

            // Mutate the target's position in-place via mutable references
            dynamics_step::<Vec3>(
                time.delta_seconds(),
                &target.params,
                control_translation,
                control_velocity,
                target_position,
                target_velocity,
            )
        }
    }
}

/// System to update target entities' rotation
pub fn dynamics_update_rotation(
    time: Res<Time>,
    mut control_point_query: Query<&mut DynamicsControlPoint<Quat>>,
    mut target_query: Query<
        (
            &mut DynamicsTarget,
            &mut DynamicsTargetState<Quat>,
            &mut Transform,
        ),
        With<DefaultDynamicsSolverTarget>,
    >,
) {
    for (target, mut target_state, mut target_transform) in target_query.iter_mut() {
        // Get the control point's position
        if let Ok(mut control_point) = control_point_query.get_mut(target.controller_entity) {
            // Flip the signs of the control quaternion if the dot products of  the two quaternions are negative
            // Since there are two valid quaternions to represent any given 3d orientation,
            // we must normalise them to prevent the system from 'taking the long way' when moving the target to match the control
            if control_point.input.dot(target_transform.rotation) < 0.0 {
                // TODO: Determine if all four or only the imaginary components of the quaternion need to be flipped (and whether we need to flip the derivative)
                control_point.input = control_point.input * -1.0;
            }

            let control_rotation = control_point.input;
            let control_angular_velocity = control_point.d_input;

            let target_rotation = &mut target_transform.rotation;
            let target_angular_velocity = &mut target_state.d_output;

            // Mutate the target's position in-place via mutable references
            dynamics_step::<Quat>(
                time.delta_seconds(),
                &target.params,
                control_rotation,
                control_angular_velocity,
                target_rotation,
                target_angular_velocity,
            )
        }
    }
}
