use bevy::prelude::*;
use std::ops::{Add, Div, Mul, Sub};

use crate::dynamics::core::{
    components::{DynamicsControlPoint, DynamicsTarget, DynamicsTargetState},
    util::DynamicsTargetParams,
};

use super::DynamicsSolverTarget;

#[derive(Component)]
pub struct MicrostepDynamicsSolverTarget;
impl DynamicsSolverTarget for MicrostepDynamicsSolverTarget {}

pub fn dynamics_step<T>(
    dt: f32,
    params: &DynamicsTargetParams,
    input: T,
    d_input: T,
    output: &mut T,
    d_output: &mut T,
) where
    T: Mul<Output = T>
        + Mul<f32, Output = T>
        + Add<T, Output = T>
        + Sub<T, Output = T>
        + Div<f32, Output = T>
        + Copy,
{
    todo!();
}

/// System to update target entities' translations
pub fn dynamics_update_translation(
    time: Res<Time>,
    control_point_query: Query<&DynamicsControlPoint<Vec3>>,
    mut target_query: Query<
        (
            &mut DynamicsTarget,
            &mut DynamicsTargetState<Vec3>,
            &mut Transform,
        ),
        With<MicrostepDynamicsSolverTarget>,
    >,
) {
    for (target, mut target_state, mut target_transform) in target_query.iter_mut() {
        // Get the control point's position
        if let Ok(control_transform) = control_point_query.get(target.controller_entity) {
            todo!();
        }
    }
}

/// System to update target entities' rotation
pub fn dynamics_update_rotation(
    time: Res<Time>,
    mut control_point_query: Query<&DynamicsControlPoint<Quat>>,
    mut target_query: Query<
        (
            &mut DynamicsTarget,
            &mut DynamicsTargetState<Quat>,
            &mut Transform,
        ),
        With<MicrostepDynamicsSolverTarget>,
    >,
) {
    for (target, mut target_state, mut target_transform) in target_query.iter_mut() {
        // Get the control point's position
        if let Ok(mut control_transform) = control_point_query.get_mut(target.controller_entity) {
            todo!();
        }
    }
}
