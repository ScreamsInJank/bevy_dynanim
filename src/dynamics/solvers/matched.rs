use bevy::prelude::*;
use std::ops::{Add, Div, Mul, Sub};

use crate::dynamics::core::{
    components::{DynamicsControlPoint, DynamicsTarget, DynamicsTargetState},
    util::DynamicsTargetParams,
};

use super::DynamicsSolverTarget;

#[derive(Component)]
pub struct MatchedDynamicsSolverTarget;
impl DynamicsSolverTarget for MatchedDynamicsSolverTarget {}

pub fn dynamics_step<T>(
    dt: f32,
    params: &DynamicsTargetParams,
    input: T,
    d_input: T,
    output: &mut T,
    d_output: &mut T,
) where
    T: Mul<Output = T>
        + Mul<f32, Output = T>
        + Add<T, Output = T>
        + Sub<T, Output = T>
        + Div<f32, Output = T>
        + Copy,
{
    // Calculate new values for k1 and k2 if the system is moving fast relative to the current timestep
    let mut k1_stable: f32;
    let mut k2_stable: f32;

    if dt * params._w < params._d {
        k1_stable = params.k1;
        k2_stable = params.k2;
    } else {
        let t1: f32 = (-params._z * params._w * dt).exp();
        let alpha: f32 = 2.0
            * t1
            * (if params._z <= 1.0 {
                (dt * params._d).cos()
            } else {
                (dt * params._d).cosh()
            });
        let beta: f32 = t1.powi(2);
        let t2: f32 = dt / (1.0 + beta - alpha);
        k1_stable = (1.0 - beta) * t2;
        k2_stable = dt * t2;
    }

    // Integrate position by velocity
    let prev_output = output.clone();
    *output = prev_output + *d_output * dt;
    // Integrate velocity by acceleration
    *d_output = (d_output.clone() * k2_stable
        + (input - output.clone() + d_input * params.k3) * dt)
        / (k2_stable + k1_stable * dt);
}

/// System to update target entities' translations
pub fn dynamics_update_translation(
    time: Res<Time>,
    control_point_query: Query<&DynamicsControlPoint<Vec3>>,
    mut target_query: Query<
        (
            &mut DynamicsTarget,
            &mut DynamicsTargetState<Vec3>,
            &mut Transform,
        ),
        With<MatchedDynamicsSolverTarget>,
    >,
) {
    for (target, mut target_state, mut target_transform) in target_query.iter_mut() {
        // Get the control point's translations
        if let Ok(control_point) = control_point_query.get(target.controller_entity) {
            let control_translation = control_point.input;
            let control_velocity = control_point.d_input;

            let target_position = &mut target_transform.translation;
            let target_velocity = &mut target_state.d_output;

            // Mutate the target's position in-place via mutable references
            dynamics_step::<Vec3>(
                time.delta_seconds(),
                &target.params,
                control_translation,
                control_velocity,
                target_position,
                target_velocity,
            )
        }
    }
}

/// System to update target entities' rotation
pub fn dynamics_update_rotation(
    time: Res<Time>,
    mut control_point_query: Query<&mut DynamicsControlPoint<Quat>>,
    mut target_query: Query<
        (
            &mut DynamicsTarget,
            &mut DynamicsTargetState<Quat>,
            &mut Transform,
        ),
        With<MatchedDynamicsSolverTarget>,
    >,
) {
    for (target, mut target_state, mut target_transform) in target_query.iter_mut() {
        // Get the control point's position
        if let Ok(mut control_point) = control_point_query.get_mut(target.controller_entity) {
            // Flip the signs of the control quaternion if the dot products of  the two quaternions are negative
            // Since there are two valid quaternions to represent any given 3d orientation,
            // we must normalise them to prevent the system from 'taking the long way' when moving the target to match the control
            if control_point.input.dot(target_transform.rotation) < 0.0 {
                // TODO: Determine if all four or only the imaginary components of the quaternion need to be flipped (and whether we need to flip the derivative)
                control_point.input = control_point.input * -1.0;
            }

            let control_rotation = control_point.input;
            let control_angular_velocity = control_point.d_input;

            let target_rotation = &mut target_transform.rotation;
            let target_angular_velocity = &mut target_state.d_output;

            // Mutate the target's position in-place via mutable references
            dynamics_step::<Quat>(
                time.delta_seconds(),
                &target.params,
                control_rotation,
                control_angular_velocity,
                target_rotation,
                target_angular_velocity,
            )
        }
    }
}
