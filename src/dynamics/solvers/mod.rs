// Dynamics solver implemented using gain clamping to maintain stability under long timesteps
pub mod default;

// Dynamics solver implemented using microsteps to maintain system stability under long timesteps (worse performance)
pub mod microstep;

// Dynamics solver implemented using zero pole matching (matched Z transform) to improve stability at high system speeds (worse performance)
pub mod matched;

pub trait DynamicsSolverTarget {}
