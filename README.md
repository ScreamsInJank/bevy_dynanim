# bevy_dynanim

This crate aims to provide a modular and powerful bevy plugin for second order dynamics and keyframeless procedural animation.

This is absolutely not production ready yet, **use in your projects at your own risk**!